import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SubscriptionComponent} from './subscription/subscription.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ApiService} from './services/api.service';
import {SubscriptionService} from './services/subscription.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SubscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    SubscriptionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
