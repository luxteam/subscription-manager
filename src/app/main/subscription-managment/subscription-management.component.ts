import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscriptionService} from '../../services/subscription.service';

@Component({
  selector: 'app-subscription-management',
  templateUrl: './subscription-management.component.html',
  styleUrls: ['./subscription-management.component.scss']
})
export class SubscriptionManagementComponent implements OnInit {
  public emailExists: boolean;
  public email: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private subscriptionService: SubscriptionService) {
    this.emailExists = this.subscriptionService.isEmailExists;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];

      if (!this.email) {
        this.router.navigate(['/']);
      }
    });
  }

  public unsubscribe(email): void {
    this.subscriptionService.unSubscribe({email: email}).subscribe(
      () => {
        this.router.navigate(['/']);
      },
      () => {
        this.router.navigate(['/']);
      }
    );
  }
}
