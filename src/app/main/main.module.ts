import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscriptionManagementComponent} from './subscription-managment/subscription-management.component';
import {MainRoutingModule} from './main.routing.module';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
  ],
  declarations: [
    SubscriptionManagementComponent
  ],
  exports: [
    SubscriptionManagementComponent
  ]
})
export class MainModule {
}
