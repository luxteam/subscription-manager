import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SubscriptionManagementComponent} from './subscription-managment/subscription-management.component';

export const routes: Routes = [
  {
    path: '', children: [
      {path: '**', component: SubscriptionManagementComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
