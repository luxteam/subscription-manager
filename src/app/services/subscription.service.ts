import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable, Subject} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {SubscriptionResponse} from '../models/subscription-response.model';
import {UnSubscriptionResponse} from '../models/unsubscription-response.model';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  public isEmailExists = true;
  public subscriptionsAmount$: Observable<number>;
  private subscriptionsAmount: Subject<number>;

  constructor(private apiService: ApiService) {
    this.subscriptionsAmount = new Subject<number>();
    this.subscriptionsAmount$ = this.subscriptionsAmount.asObservable();
  }

  public get(email: string): Observable<boolean> {
    return this.apiService.get<boolean>('/subscription', new HttpParams().append('email', email)).pipe(
      map(
        value => {
          console.log('SubscriptionService.get', value);
          return value;
        }
      )
    );
  }

  public getSubscriptionsAmount(): void {
    this.apiService.get<number>('/subscription/amount').subscribe(
      (body: any) => {
        this.subscriptionsAmount.next(body.amount);
      }
    );
  }

  public subscribe(body: any): Observable<SubscriptionResponse> {
    return this.apiService.post('/subscription', body).pipe(
      map(
        (response: SubscriptionResponse) => {
          if (response) {
            this.isEmailExists = false;
            this.subscriptionsAmount.next(response.totalSubscriptions);
          }

          return response;
        }
      )
    );
  }

  public unSubscribe(body: any): Observable<UnSubscriptionResponse> {
    return this.apiService.post('/subscription/unsubscribe', body).pipe(
      map(
        (response: UnSubscriptionResponse) => {
          if (response) {
            this.subscriptionsAmount.next(response.totalSubscriptions);
          }

          return response;
        }
      )
    );
  }
}
