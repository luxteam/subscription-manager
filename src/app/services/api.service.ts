import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {throwError} from 'rxjs/internal/observable/throwError';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {
  private readonly headers: HttpHeaders;
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    // this.headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.apiUrl = environment.apiUrl;
  }

  get<T>(path: string, params: HttpParams = new HttpParams()): Observable<T> {
    return this.http.get(`${this.apiUrl}${path}`, {headers: this.headers, params: params})
      .pipe(
        map(
          response => {
            return <T>response;
          }),
        catchError(
          (error) => {
            return throwError(error);
          }
        )
      );
  }

  post<T>(path: string, body: any): Observable<any> {
    return this.http.post(`${this.apiUrl}${path}`, body, {headers: this.headers})
      .pipe(
        map(
          response => {
            return <T>response;
          }),
        catchError(
          (error) => {
            return throwError(error);
          }
        )
      );
  }

  put(path: string, model: any): Observable<any> {
    const body = JSON.stringify(model);

    return this.http.put(`${this.apiUrl}${path}`, body, {headers: this.headers});
  }

  delete(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.delete(`${this.apiUrl}${path}`, {headers: this.headers, params: params});
  }
}
