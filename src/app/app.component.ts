import {Component, OnInit} from '@angular/core';
import {SubscriptionService} from './services/subscription.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public amount = 0;

  constructor(private subscriptionService: SubscriptionService) {
  }

  public ngOnInit() {
    this.subscriptionService.subscriptionsAmount$.subscribe(
      (value: number) => {
        this.amount = value;
      }
    );
  }
}
