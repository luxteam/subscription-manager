export class SubscriptionResponse {
  totalSubscriptions: number;
  isValid: boolean;
  email: string;
}
