export class UnSubscriptionResponse {
  success: string;
  totalSubscriptions: number;
}
