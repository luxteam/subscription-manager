import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SubscriptionService} from '../services/subscription.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  public subscriptionForm: FormGroup;

  constructor(private router: Router,
              private subscriptionService: SubscriptionService) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptionService.getSubscriptionsAmount();
  }

  private createForm(): void {
    this.subscriptionForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        // tslint:disable-next-line:max-line-length
        Validators.pattern('^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'),
        Validators.minLength(4),
      ]),
    });
  }

  public subscriptionFormSubmit(email: string): void {
    this.subscriptionService.subscribe({email: email}).subscribe(
      () => {
        this.router.navigate(['/main'], {queryParams: {email: email}});
      },
      () => {
        this.router.navigate(['/main'], {queryParams: {email: email}});
      }
    );

    console.log(email);
  }
}
